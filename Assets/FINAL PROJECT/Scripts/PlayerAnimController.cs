﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PlayerAnimController : MonoBehaviour
{
    private Animator anim;
    private bool walking;
    public NavMeshAgent navMeshAgent;
    private Sword swordScript;
    public bool attacking;

	// Use this for initialization
	void Awake ()
    {
        anim = GetComponent<Animator>();
        navMeshAgent = GetComponent<NavMeshAgent>();
        swordScript = GameObject.FindGameObjectWithTag("Sword").GetComponent<Sword>();
	}
	
	// Update is called once per frame
	void Update ()
    {
		if(navMeshAgent.velocity != Vector3.zero)
        {
            walking = true;
        }

        else
        {
            walking = false;
        }

        anim.SetBool("IsWalking", walking);
        anim.SetBool("IsAttacking", attacking);
	}

    public void Attack()
    {
        attacking = true;
    }

    //SwordHit is called by the Attack01 Animation Event
    public void SwordHit()
    {
        swordScript.SwordHit();
        print("This is an animation event for the sword!");
    }
}

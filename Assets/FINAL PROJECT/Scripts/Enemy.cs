﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : NPC
{

    public int enemyHealth = 4;
    private PlayerAnimController playerAnimController;
    private AudioSource audio;
    public ParticleSystem dust;
    private Transform dustContainer;


    void Start()
    {
        playerAnimController = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerAnimController>();
        audio = GetComponent<AudioSource>();
        dustContainer = this.gameObject.transform.GetChild(0);
        dust = dustContainer.GetComponent<ParticleSystem>();
    }


    public override void Interact()
    {
        //playerAnimController.attacking = true;
        print("Interacting with Enemy.");
        playerAnimController.Attack();
        
    }


    public void TakeDamage(int damageAmount)
    {
        audio.Stop();
        audio.Play();
        enemyHealth -= damageAmount;
        //var em = dust.emission;
        // em.enabled = true;
        dust.Play();

        if (enemyHealth <= 0)
        {
            playerAnimController.attacking = false;
            //yield return new WaitWhile(hitAudio.);
            Destroy(gameObject, audio.clip.length);
        }

        else
        {
            //playerAnimController.Attack();
        }
    }
}
